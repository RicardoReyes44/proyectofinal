'''
Created on 31 oct. 2020

@author: RSSpe
'''

class PilaLetras:
    "" "Clase para gestionar letras en una pila" ""

    def __init__(self):
        self.__letrasApiladas = []


    @property
    def letrasApiladas(self):
        return self.__letrasApiladas


    def agregarLetra(self, letra:str) -> None:
        "" "Metodo para agregar letras" ""
        self.__letrasApiladas.append(letra)
