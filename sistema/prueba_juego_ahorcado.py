'''
Created on 10 oct. 2020

@author: RSSpe
'''

from .archivo import Archivo
from .juego_ahorcado import JuegoAhorcado


class PruebaJuegoAhorcado:
    "" "Clase para administrar el programa" ""

    def __init__(self):
        self.__archivo : Archivo = Archivo()
        self.__candado : boolean = True
        self.__palabras : list = self.__archivo.accederArchivo() 

    def ejecutar(self) -> None:
        "" "Funcion para ejecutar el programa" ""
        self.menuOpciones()


    def detener(self) -> None:
        "" "Funcion para detener el programa" ""
        self.__candado = False


    def menuOpciones(self) -> None:
        "" "Metodo para poder elegir entre opciones principales" ""
        while(self.__candado):
            print("-------------------Menu-------------------")
            print("1) Verificar archivo")
            print("2) Llenar archivo con palabras")
            print("3) Borrar archivo")
            print("4) Jugar")
            print("5) Salir\n")

            try:

                opcion = int(input("Introduce una opcion: "))

                if opcion==1:

                    print(f"\nPalabras en español: {self.__palabras[0]}")
                    print(f"Palabras en ingles: {self.__palabras[1]}")
                    print(f"Cantidad de palabras: {len(self.__palabras[0]) + len(self.__palabras[1])}")

                elif opcion==2:
                    self.__archivo.escribirEnArchivo()
                    self.__palabras = self.__archivo.accederArchivo() 

                elif opcion==3:
                    self.__archivo.eliminarArchivo()
                    self.__palabras = [[], []]

                elif opcion==4:
                    juego = JuegoAhorcado(self.__palabras, self.__archivo)
                    juego.hangman()
                    self.__palabras = juego.palabras

                elif opcion==5:
                    self.detener()
                    print("\n------------------------\nPrograma terminado    :)\n------------------------")

                else:
                    print("\nOpcion inexistente, por favor vuelve a intentarlo")
                
            except ValueError as error:
                print(f"\nError en la entrada de datos<{error}>, por favor vuelve a intentarlo")
            print()
