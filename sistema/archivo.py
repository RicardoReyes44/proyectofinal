'''
Created on 10 oct. 2020

@author: RSSpe
'''

from abc import ABC, abstractmethod
from .config import RUTA
from os import remove
import os
import codecs


class Archivo():
    "" "Clase para gestionar nuestro archivo" ""

    def quickSort(self, arreglo, izq, der) -> None:
        "" "Metodo para ordenar lista con el algoritmo quicksort" ""

        pivote = arreglo[izq]
        i = izq
        j = der
        aux = 0

        while i<j:
            while arreglo[i]<=pivote and i<j:
                i+=1
            while arreglo[j]>pivote:
                j-=1
            if i<j:
                aux = arreglo[i]
                arreglo[i] = arreglo[j]
                arreglo[j] = aux

        arreglo[izq] = arreglo[j]
        arreglo[j] = pivote
        if izq<j-1:
            self.quickSort(arreglo, izq, j-1)
        if j+1<der:
            self.quickSort(arreglo, j+1, der)


    def accederArchivo(self) -> list:
        "" "Metodo para cargar palabras y acceder a un archivo" ""

        palabrasLeidas : list = []
        ingles : list = []
        español : list = []
        palabras : File = None

        try:
            self.crearArchivo()

            palabras = codecs.open(RUTA, 'r', 'utf-8')
            palabrasLeidas = palabras.read().upper()
            palabrasLeidas = palabrasLeidas.replace(" ", "")

            if palabrasLeidas[len(palabrasLeidas)-1]=='/' and palabrasLeidas[0]=='/':
                palabrasLeidas = [[], []]

            elif palabrasLeidas[0]=='/':
                ingles = palabrasLeidas.split("/")[1].split(",")
                self.quickSort(ingles, 0, len(ingles)-1)
                palabrasLeidas = [[], ingles]

            elif palabrasLeidas[len(palabrasLeidas)-1]=='/':
                español = palabrasLeidas.split("/")[0].split(",")
                self.quickSort(español, 0, len(español)-1)
                palabrasLeidas = [español, []]

            else:
                español = palabrasLeidas.split("/")[0].split(",")
                ingles = palabrasLeidas.split("/")[1].split(",")
                self.quickSort(español, 0, len(español)-1)
                self.quickSort(ingles, 0, len(ingles)-1)
                palabrasLeidas = [español, ingles]

        except EOFError as error:
            print("Error en la lectura <", error,">")

        except ImportError as error:
            print("Error en la importacion <", error,">")

        except FileNotFoundError as error:
            print("Error no se encontro el archivo <", error,">")

        except IndexError as error:
            palabrasLeidas = [[], []]
        
        finally:
            palabras.close()

        return palabrasLeidas


    def escribirEnArchivo(self) -> None:
        "" "Metodo para escribir sobre una archivo" ""
        self.crearArchivo()
        archivo : File = None
        
        print("Introduce palabras con el formato:\n(palabras en español "
            + "separadas por comas)/(palabras en ingles separadas por comas)"
            + "\nEj: camaron,azul/car,sky,blue\n")
        texto = input("Texto = ")

        try:
            archivo = codecs.open(RUTA, 'w', 'utf-8')
            archivo.write(texto)

        except EOFError as error:
            print("Error en la lectura <", error,">")

        except ImportError as error:
            print("Error en la importacion <", error,">")

        except FileNotFoundError as error:
            print("Error no se encontro el archivo <", error,">")
        
        finally:
            archivo.close()


    def eliminarArchivo(self) -> None:
        "" "Metodo para eliminar un archivo" ""

        remove(RUTA)
        print("Se elimino el archivo, se creara uno nuevo...")
        self.crearArchivo()


    def crearArchivo(self) -> None:
        "" "Metodo para crear un archivo" ""

        if not os.path.isfile(RUTA):
            palabras = codecs.open(RUTA, 'w', 'utf-8')
            palabras.close()
            print("Archivo creado")

