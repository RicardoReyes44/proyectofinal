'''
Created on 16 oct. 2020

@author: RSSpe
'''

class Oportunidades:
    "" "Clase para gestionar las oportunidades" ""

    def __init__(self):
        self.__cantidad : int = 0


    @property
    def cantidad(self):
        return self.__cantidad


    def incrementarErrores(self) -> None:
        "" "Metodo para sumar errores cometidos" ""
        self.__cantidad+=1
