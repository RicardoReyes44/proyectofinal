'''
Created on 10 oct. 2020

@author: RSSpe
'''

from .archivo import Archivo
from .oportunidades import Oportunidades
from sistema.pila_letras import PilaLetras
import random
import re


class JuegoAhorcado:
    "" "Clase para crear nuestro juego" ""
    
    def __init__(self, palabras, archivo):
        self.__archivo : Archivo = archivo
        self.__palabras : list = palabras
        self.__idioma : int = None


    @property
    def palabras(self):
        return self.__palabras


    @property
    def archivo(self):
        return self.__archivo


    def cargarPalabras(self) -> list:
        "" "Metodo para cargar palabras desde un archivo de texto" ""
        self.__palabras = self.__archivo.accederArchivo()

        return self.__palabras


    def elegirPalabra(self, palabras:list) -> str:
        "" "Metodo para elegir una palabra" ""

        palabra : str = ""

        if palabras!=[[], []]:
            if len(palabras[0])!=0 and len(palabras[1])!=0:

                self.__idioma = random.randint(0, 1)

                if self.__idioma==0:
                    palabra = palabras[0][random.randint(0, len(palabras[0])-1)]
                else:
                    palabra = palabras[1][random.randint(0, len(palabras[1])-1)]

            elif len(palabras[0])!=0:
                palabra = palabras[0][random.randint(0, len(palabras[0])-1)]
                self.__idioma = 0

            elif len(palabras[1])!=0:
                palabra = palabras[1][random.randint(0, len(palabras[1])-1)]
                self.__idioma = 1

        return palabra


    def seAdivinoLaPalabra(self, palabraSecreta:str, letrasIngresadas:list) -> bool:
        "" "Metodo para saber si se ha adivinado la palabra" ""
        puntos : int = 0 

        for i in letrasIngresadas:
            for j in palabraSecreta:
                if i==j:
                    puntos+=1

        if puntos==len(palabraSecreta):
            return True
        else:
            return False


    def obtenerPalabraAdivinada(self, palabraSecreta:str, letrasIngresadas:list, erroresCometidos:int) -> str:
        "" "Metodo para descubrir la palabra secreta" ""

        palabra : str = ""
        aparece : int = 0
        
        for i in palabraSecreta:
            if i==letrasIngresadas[len(letrasIngresadas)-1]:
                aparece+=1

        if aparece==0:
            erroresCometidos.incrementarErrores()

        for i in palabraSecreta:
            punto = 0
            for j in letrasIngresadas:
                if i==j:
                    punto+=1
                    break

            if punto!=0:
                palabra+=i
            else:
                palabra+=" _ "

        return palabra


    def obtenerLetrasDisponibles(self, letrasIngresadas:list):
        "" "Metodo para obtener las letras que no han sido ingresadas" ""

        abcdario : str = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"

        if self.__idioma==1:
            abcdario = abcdario.replace("Ñ", "")

        for i in letrasIngresadas:
            if self.busquedaBinaria(abcdario, i, 0, len(abcdario)-1):
                abcdario = abcdario.replace(i, "")

        return abcdario


    def hangman(self):
        "" "Metodo de rectificacion e inicio del juego" ""

        if len(self.__palabras[0]) + len(self.__palabras[1])!=0:
            self.inicioAhorcado(self.elegirPalabra(self.__palabras))
        else:
            palabras = None
            
            while len(self.__palabras[0]) + len(self.__palabras[1])==0:
                print("\nNo hay palabras, por favor ingresa algunas\n")
                self.__archivo.escribirEnArchivo()
                palabras = self.cargarPalabras()
                print()

            self.inicioAhorcado(self.elegirPalabra(palabras))


    def inicioAhorcado(self, palabraSecreta:str) -> None:
        "" "Metodo para gestionar nuesto juego" ""
        
        erroresCometidos : Oportunidades = Oportunidades()
        vivo : bool = True
        letrasIngresadas : PilaLetras = PilaLetras()
        existe : bool = False

        print()

        while(vivo):
            
            if self.__idioma==1:
                print("La palabra esta en ingles")
            else:
                print("La palabra esta en español")

            print("Tienes", 10-erroresCometidos.cantidad, "vidas")
            print("Longitud de la palabra:", len(palabraSecreta), "\n")
            print("Letras disponibles:", self.obtenerLetrasDisponibles(letrasIngresadas.letrasApiladas))

            letra = input("Introduce una letra: ").upper()
            
            if len(letra)==1:
                if re.search("[A-ZÑ]", letra) and self.__idioma==0 or re.search("[A-Z]", letra) and self.__idioma==1:
                    for i in letrasIngresadas.letrasApiladas:
                        if letra==i:
                            existe = True
                            break
                    
                    if not existe:
                        letrasIngresadas.agregarLetra(letra)

                        print("Palabra secreta:", self.obtenerPalabraAdivinada(palabraSecreta, letrasIngresadas.letrasApiladas, erroresCometidos))

                        if erroresCometidos.cantidad==10:
                            print("\n-------------------\nHaz perdido\n\nLa palabra secreta era:", palabraSecreta,"\n-------------------\n")
                            vivo=False

                        elif self.seAdivinoLaPalabra(palabraSecreta, letrasIngresadas.letrasApiladas):
                            print("\n-------------------\nHaz ganado\n-------------------\n")
                            vivo=False

                    else:
                        print("Por favor, prueba con una letra que no haya sido ingresada")
                else:
                    print("No puedes incluir caracteres invalidos, por favor vuelve a intentarlo\n")

            else:
                print("Solo puedes ingresar una letra a la vez, por favor vuelve a intentarlo\n")
            existe = False
            print("------------------------------------------------")


    def busquedaBinaria(self, array:str, buscado:str, primero:int, ultimo:int) -> bool:
        "" "Metodo para buscar una letra en una cadena" ""

        if primero>ultimo:
            return False
        else:
            
            mitad : int = (primero+ultimo)//2
            
            if buscado==array[mitad]:
                return True
            elif buscado<array[mitad]:
                return self.busquedaBinaria(array, buscado, primero, mitad-1)
            else:
                return self.busquedaBinaria(array, buscado, mitad+1, ultimo)
