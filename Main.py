'''
Created on 10 oct. 2020

@author: RSSpe
'''

from sistema.prueba_juego_ahorcado import PruebaJuegoAhorcado


def main() -> None:
    "" "Funcion para empezar el programa" ""

    prueba : PruebaJuegoAhorcado = PruebaJuegoAhorcado()
    prueba.ejecutar()


if __name__ == '__main__':
    main()
